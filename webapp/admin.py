from django.contrib import admin

from .models import Transactions, AppConstants

admin.site.register(Transactions)

admin.site.register(AppConstants)

# Register your models here.
