from django.db import models

# Create your models here.

class Transactions(models.Model):
    email = models.EmailField(max_length=100)
    vote_numbers = models.IntegerField()
    payment_success = models.BooleanField(default=False)
    vote_for = models.CharField(max_length=16)
    uid = models.CharField(max_length=100)
    vote_price = models.DecimalField(max_digits=6, decimal_places=2, default=1)
    total_pay = models.DecimalField(max_digits=6, decimal_places=2, default=0)


class AppConstants(models.Model):
    vote_price = models.DecimalField(max_digits=6, decimal_places=2)
    bottom_text = models.TextField(default='')
    about_us_text = models.TextField(default='')
    disable_voting = models.BooleanField(default=False)
