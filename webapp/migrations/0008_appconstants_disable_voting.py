# Generated by Django 4.1.6 on 2023-02-22 16:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0007_appconstants_about_us_text'),
    ]

    operations = [
        migrations.AddField(
            model_name='appconstants',
            name='disable_voting',
            field=models.BooleanField(default=False),
        ),
    ]
