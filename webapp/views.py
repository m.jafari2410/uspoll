from django.shortcuts import render, redirect
from django.conf import settings
import stripe
from webapp.models import Transactions, AppConstants
import uuid
from django.urls import reverse
from django.db.models import Sum
from dotenv import load_dotenv
import os


def create_checkout_session(request):
    app_constants = AppConstants.objects.first()
    voting_disabled = app_constants.disable_voting
    if voting_disabled:
        return redirect(reverse('index'))

    load_dotenv()
    DOMAIN = str(os.getenv('DOMAIN'))
    stripe.api_key = str(os.getenv('STRIPE_PRIVATE_KEY'))

    data = request.POST
    votes_number = data.get('votes_number')
    vote_for = data.get('vote_for')

    unit_amount = AppConstants.objects.first().vote_price

    transaction = Transactions.objects.create(
        email = data.get('email'),
        vote_numbers = votes_number,
        vote_for = vote_for,
        uid = str(uuid.uuid1()),
        vote_price = unit_amount,
        total_pay = float(unit_amount) * float(votes_number)
    )

    session = stripe.checkout.Session.create(
        payment_method_types=['card'],
        line_items=[{
        'price_data': {
        'currency': 'usd',
        'product_data': {
        'name': f'{votes_number} votes for {vote_for}',
        },
        'unit_amount': int(unit_amount * 100),
        },
        'quantity': votes_number,
        }],
        mode='payment',
        success_url=DOMAIN + f'/success?tr={transaction.uid}',
        cancel_url=DOMAIN + '',
    )
    return redirect(session.url, code=303)


def index(request):
    tr_id = request.GET.get('tr')
    if (tr_id is not None):
        transaction = Transactions.objects.get(id=tr_id)
        success_transaction = {
            'vote_for': transaction.vote_for,
            'vote_numbers': transaction.vote_numbers
        }
    else:
        success_transaction = None

    app_constants = AppConstants.objects.first()
    unit_amount = app_constants.vote_price
    bottom_text = app_constants.bottom_text
    voting_disabled = app_constants.disable_voting
    
    trump_votes = Transactions.objects.filter(payment_success=True, vote_for="Trump").aggregate(Sum('vote_numbers'))['vote_numbers__sum']
    biden_votes = Transactions.objects.filter(payment_success=True, vote_for="Biden").aggregate(Sum('vote_numbers'))['vote_numbers__sum']
    trump_percentage = round(trump_votes / (trump_votes + biden_votes) * 100, 1)
    biden_percentage = round(biden_votes / (trump_votes + biden_votes) * 100, 1)
    return render(request, 'webapp/index.html', {'unit_price': unit_amount, 'trump_votes': trump_votes, 'biden_votes': biden_votes, 'transaction': success_transaction, 'bottom_text': bottom_text, 'trump_percentage': trump_percentage, 'biden_percentage': biden_percentage, 'voting_disabled': voting_disabled})


def success_payment(request):
    tr_uid = request.GET.get('tr')
    transaction = Transactions.objects.get(uid=tr_uid)
    if (not transaction.payment_success):
        transaction.payment_success=True
        transaction.save()
        return redirect(reverse('index') + f'?tr={transaction.id}')
    return redirect(reverse('index'))


def about_us(request):
    app_constants = AppConstants.objects.first()
    about_us_text = app_constants.about_us_text
    return render(request, 'webapp/about_us.html', {'about_us_text': about_us_text})
