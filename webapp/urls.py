from unicodedata import name
from django.urls import path

from webapp import views

urlpatterns = [
    path('', views.index, name="index"),
    path('checkout/', views.create_checkout_session, name="checkout"),
    path('success/', views.success_payment, name="success_payment"),
    path('about-us/', views.about_us, name="about_us")
]